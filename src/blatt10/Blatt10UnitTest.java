package blatt10;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Diese Tests sind Bruteforce Tests und beachten bei weitem nicht alle cases.
Mit randomPermutations() steht zwar ein Test zur Verfügung, der zufällige Kombinationen erzeugt und prüft,
dieser kann unter Umständen jedoch nicht alle fehlererzeugenden Kombinationen erfassen. Ein Mehrfachausführen
bzw. erhöhen der Kombinationszahlen, sollte jedoch mit hoher Wahrscheinlich vorhandene Fehler aufdecken.
 */

public class Blatt10UnitTest {
    // region Simulate Tests
    @org.junit.Test
    public void testSample1() throws IOException {
        String tmString = "01\n" + // Alphabet
                "init\n" + // Start state
                "fin\n" + // Finite states
                "init;0;init;1;R\n" + // Transitions...
                "init;1;init;0;R\n" +
                "init;█;fin;1;R\n" +
                "END";

        BufferedReader tmReader = new BufferedReader(new StringReader(tmString));
        TuringMachine<String> tm = TuringMachine.parse(tmReader);

        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "1", 2));
        assertEquals(Simulate.Result.RUNNING,
                Simulate.simulate(tm, "000000000", 5));
    }

    @org.junit.Test
    public void testSample2() throws IOException {
        String tmString = "01\n" +
                "init\n" +
                "fin\n" +
                "flip;0;flip;0;L\n" +
                "flip;1;init;0;L\n" +
                "init;0;flip;1;R\n" +
                "init;1;init;1;R\n" +
                "init;█;fin;1;R\n" +
                "END";

        BufferedReader tmReader = new BufferedReader(new StringReader(tmString));
        TuringMachine<String> tm = TuringMachine.parse(tmReader);

        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "11", 10));
        assertEquals(Simulate.Result.STUCK,
                Simulate.simulate(tm, "1110", 20));
        assertEquals(Simulate.Result.RUNNING,
                Simulate.simulate(tm, "100000", 1000));
    }

    @org.junit.Test
    public void testUnaryInc() throws IOException {
        String tmString = "1\n" +
                "q\n" +
                "f\n" +
                "q;█;f;1;N\n" +
                "q;1;q;1;R\n" +
                "END";

        BufferedReader tmReader = new BufferedReader(new StringReader(tmString));
        TuringMachine<String> tm = TuringMachine.parse(tmReader);

        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "", 1));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "1", 2));
        assertEquals(Simulate.Result.STUCK,
                Simulate.simulate(tm, "2", 10));
        assertEquals(Simulate.Result.RUNNING,
                Simulate.simulate(tm, "1", 0));
    }

    @org.junit.Test
    public void testEvenLengthWords() throws IOException {
        String tmString = "1\n" +
                "even\n" +
                "f\n" +
                "even;█;f;█;N\n" +
                "even;1;odd;1;R\n" +
                "odd;1;even;1;R\n" +
                "END";

        BufferedReader tmReader = new BufferedReader(new StringReader(tmString));
        TuringMachine<String> tm = TuringMachine.parse(tmReader);

        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "", 1));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "11", 3));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "1111", 5));
        assertEquals(Simulate.Result.STUCK,
                Simulate.simulate(tm, "2", 10));
        assertEquals(Simulate.Result.STUCK,
                Simulate.simulate(tm, "1", 2));
        assertEquals(Simulate.Result.RUNNING,
                Simulate.simulate(tm, "1", -5));
    }

    @org.junit.Test
    public void testEmptyLanguage() throws IOException {
        String tmString = "1\n" +
                "q\n" +
                "\n" +
                // Continuously go right/left and flip letters.
                "q;█;p;1;R\n" +
                "q;1;p;█;R\n" +
                "p;█;q;1;L\n" +
                "p;1;q;█;L\n" +
                "END";

        BufferedReader tmReader = new BufferedReader(new StringReader(tmString));
        TuringMachine<String> tm = TuringMachine.parse(tmReader);

        assertEquals(Simulate.Result.STUCK,
                Simulate.simulate(tm, "2", 10));
        assertEquals(Simulate.Result.RUNNING,
                Simulate.simulate(tm, "", 2));
        assertEquals(Simulate.Result.RUNNING,
                Simulate.simulate(tm, "11", 3));
        assertEquals(Simulate.Result.RUNNING,
                Simulate.simulate(tm, "1111", 5));
        assertEquals(Simulate.Result.RUNNING,
                Simulate.simulate(tm, "1", 10));
    }

    @org.junit.Test
    public void testAllAccepted() throws IOException {
        String tmString = "\n" +
                "f\n" +
                "f\n" +
                "END";

        BufferedReader tmReader = new BufferedReader(new StringReader(tmString));
        TuringMachine<String> tm = TuringMachine.parse(tmReader);

        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "2", 10));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "", 2));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "x", 3));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "1111", 5));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "1", 2));
    }

    @org.junit.Test
    public void testIntegerTM() {
        Set<Character> alphabet = Stream.of('a', 'b').collect(Collectors.toSet());
        Integer initialState = -1;
        TuringMachine<Integer> tm = new TuringMachine<>(alphabet, initialState);

        Integer finalState = 42;
        tm.addFinalState(finalState);

        // Accept words starting with 'a'.
        tm.addTransition(initialState, 'a', finalState, 'a',
                TuringMachine.Direction.N);

        assertEquals(Simulate.Result.STUCK,
                Simulate.simulate(tm, "2", 10));
        assertEquals(Simulate.Result.STUCK,
                Simulate.simulate(tm, "", 2));
        assertEquals(Simulate.Result.STUCK,
                Simulate.simulate(tm, "x", 3));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "a", 1));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "aaa", 1));
    }

    @org.junit.Test
    public void testBinaryInc() throws IOException {
        // Taken from lecture slide 252/253.
        String tmString = "01\n" +
                "q0\n" +
                "qf\n" +
                "q0;0;q0;0;R\n" +
                "q0;1;q0;1;R\n" +
                "q0;█;q1;█;L\n" +
                "q1;1;q1;0;L\n" +
                "q1;0;q2;1;L\n" +
                "q1;█;qf;1;N\n" +
                "q2;0;q2;0;L\n" +
                "q2;1;q2;1;L\n" +
                "q2;█;qf;█;R\n" +
                "END";

        BufferedReader tmReader = new BufferedReader(new StringReader(tmString));
        TuringMachine<String> tm = TuringMachine.parse(tmReader);

        assertEquals(Simulate.Result.STUCK,
                Simulate.simulate(tm, "2", 1));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "", 3));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "101", 10));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "1111", 20));
        assertEquals(Simulate.Result.ACCEPTED,
                Simulate.simulate(tm, "1", 5));
        assertEquals(Simulate.Result.RUNNING,
                Simulate.simulate(tm, "1", 3));
    }


    @Test
    void empty() {
        TuringMachine tm = Rectangle.rectangleMachine();
        assertEquals(Simulate.simulate(tm, "", 1000000), Simulate.Result.STUCK);
    }

    @Test
    private void initial() {
        TuringMachine tm = Rectangle.rectangleMachine();
        assertEquals(Simulate.simulate(tm, "^<v", 1000000), Simulate.Result.STUCK);
        assertEquals(Simulate.simulate(tm, "><v", 1000000), Simulate.Result.STUCK);
        assertEquals(Simulate.simulate(tm, ">^v", 1000000), Simulate.Result.STUCK);
        assertEquals(Simulate.simulate(tm, ">^<", 1000000), Simulate.Result.STUCK);
        assertEquals(Simulate.simulate(tm, "><", 1000000), Simulate.Result.STUCK);
        assertEquals(Simulate.simulate(tm, "^v", 1000000), Simulate.Result.STUCK);
    }
    // endregion

    //region RectangleTM
    @Test
    void allImpossibleInOrder() {
        TuringMachine tm = Rectangle.rectangleMachine();

        for (int k = 0; k < 200; k++) {
            for (int x = 0; x <= k; x++) {
                for (int y = 0; y <= k; y++) {
                    for (int z = 0; z <= k; z++) {
                        for (int w = 0; w <= k; w++) {

                            if (x + y + z + w != k) continue;
                            if (x == z && y == w) continue;

                            System.out.println("Length:" + k + ", " + x + ":" + y + ":" + z + ":" + w);

                            StringBuilder sb = new StringBuilder();

                            for (int i = 0; i < x; i++) {
                                sb.append('>');
                            }

                            for (int i = 0; i < y; i++) {
                                sb.append('^');
                            }

                            for (int i = 0; i < z; i++) {
                                sb.append('<');
                            }

                            for (int i = 0; i < w; i++) {
                                sb.append('v');
                            }

                            assertEquals(Simulate.simulate(tm, sb.toString(), 1000000), Simulate.Result.STUCK);

                        }
                    }
                }
            }
        }
    }

    @Test
    void allPossible() {
        TuringMachine tm = Rectangle.rectangleMachine();

        for (int x = 1; x < 100; x++) {
            for (int y = 1; y < 100; y++) {
                if (x + y > 100) continue;

                System.out.println(x + ":" + y);

                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < x; i++) {
                    sb.append('>');
                }

                for (int i = 0; i < y; i++) {
                    sb.append('^');
                }

                for (int i = 0; i < x; i++) {
                    sb.append('<');
                }

                for (int i = 0; i < y; i++) {
                    sb.append('v');
                }

                assertEquals(Simulate.simulate(tm, sb.toString(), 1000000), Simulate.Result.ACCEPTED);
            }
        }
    }

    @Test
    void randomPermutations() {
        TuringMachine tm = Rectangle.rectangleMachine();

        Random rand = new Random();

        for (int k = 1; k < 200; k++) {
            for (int x = 0; x < (k < 10 ? 10000 : 1000); x++) {
                StringBuilder sb = new StringBuilder();

                int i = 0;
                int j = 0;
                int l = 0;
                int m = 0;

                boolean accepted = true;

                for (int y = 0; y < k; y++) {
                    int choice = rand.nextInt(4);

                    switch (choice) {
                        case 0:
                            sb.append(">");
                            accepted = accepted && j + l + m == 0;
                            i++;
                            break;
                        case 1:
                            sb.append("^");
                            accepted = accepted && i > 0 && j + m == 0;
                            l++;
                            break;
                        case 2:
                            sb.append("<");
                            accepted = accepted && i > 0 && l > 0 && m == 0;
                            j++;
                            break;
                        case 3:
                            sb.append("v");
                            accepted = accepted && i > 0 && j > 0 && l > 0;
                            m++;
                            break;
                    }
                }

                Simulate.Result result = Simulate.simulate(tm, sb.toString(), 1000000);

                if (accepted && i == j && i > 0 && l == m && l > 0) {
                    assertEquals(Simulate.Result.ACCEPTED, result);
                } else {
                    assertEquals(Simulate.Result.STUCK, result);
                }
            }
        }
    }
    // endregion

    // region TestPrimeTM
    @Test
    void TestPrimeTM() {
        ArrayList<Integer> prims = new ArrayList<>(Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47));

        String word = "";
        for (int i = 0; i < 51; i++) {
            Simulate.Result s = Simulate.simulate(PrimeTM.primeMachine(), word, 1000000);
            if (prims.contains(word.length())) {
                assert s.equals(Simulate.Result.ACCEPTED);
            }
            else {
                assert s.equals(Simulate.Result.STUCK);
            }
            word += "a";
        }
    }
    // endregion
}
